<!DOCTYPE html>
<html lang="en">

<head>
  <title>MyPresence | Update Profile</title>
  @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Main Sidebar Container -->
    @include('template.left-sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper sidebar-dark">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Home</li>
                <li class="breadcrumb-item active">MyPresence</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                Update Profile
              </div>

              @if ($message = Session::get('success'))
              <p>{{ $message }}</p>
              @endif

              <div class="card-body">
                <form method="POST" action="{{ route('profile.edit') }}">
                  @method('patch')
                  @csrf

                  <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                    <div class="col-md-6">
                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>

                      @error('name')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="nis" class="col-md-4 col-form-label text-md-right">{{ __('NIS') }}</label>

                    <div class="col-md-6">
                      <input id="nis" type="text" class="form-control @error('nis') is-invalid @enderror" name="nis" autocomplete="nis" autofocus>

                      @error('nis')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="jeniskelamin" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>

                    <div class="col-md-6">
                      <input id="jeniskelamin" type="text" class="form-control @error('jeniskelamin') is-invalid @enderror" name="jeniskelamin" autocomplete="jeniskelamin" autofocus>

                      @error('jeniskelamin')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="ttl" class="col-md-4 col-form-label text-md-right">{{ __('TTL') }}</label>

                    <div class="col-md-6">
                      <input id="ttl" type="ttl" class="form-control @error('ttl') is-invalid @enderror" name="ttl" autocomplete="TTL">

                      @error('ttl')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="kelas" class="col-md-4 col-form-label text-md-right">{{ __('Kelas') }}</label>

                    <div class="col-md-6">
                      <input id="kelas" type="kelas" class="form-control @error('kelas') is-invalid @enderror" name="kelas" autocomplete="kelas">

                      @error('kelas')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                    <div class="col-md-6">
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email">

                      @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">
                        Update Profile
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('template.footer')
    @include('template.script')
</body>

</html>