<!DOCTYPE html>
<html lang="en">

<head>
  <title>MyPresence | Home</title>
  <!-- Embedding Poppins Font From Google Font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

  <!-- Embedding Icons From Fontawesome Icons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <link rel="stylesheet" href="{{ asset('background/style.css') }}">
  @include('template.head')
</head>


<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Main Sidebar Container -->
    @include('template.left-sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper sidebar-dark">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Home</li>
                <li class="breadcrumb-item active">MyPresence</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <div class="biodata">
        <img src="img/kosong.png">
        <h3>{{ auth()->user()->name }}</h3>
        <h6>NIS : {{ auth()->user()->nis }}</h6>
        <div class="biodata-list">
          <div class="biodataku">
            <span class="jd-biodata">Jenis Kelamin</span>
            <span>{{ auth()->user()->jeniskelamin }}</span>
          </div>
          <div class="biodataku">
            <span class="jd-biodata">TTL</span>
            <span>{{ auth()->user()->ttl }}</span>
          </div>
          <div class="biodataku">
            <span class="jd-biodata">Sekolah</span>
            <span>{{ auth()->user()->sekolah }}</span>
          </div>
          <div class="biodataku">
            <span class="jd-biodata">Email</span>
            <span>{{ auth()->user()->email }}</span>
          </div>
        </div>
        <div class="edit">
          <a href="{{ route('profile.edit') }}" class="btn btn-primary">Edit Profile</a>
        </div>
      </div><br>

      <!-- Main Footer -->
      @include('template.footer')

      <!-- ./wrapper -->



      <!-- REQUIRED SCRIPTS -->
      @include('template.script')
</body>

</html>