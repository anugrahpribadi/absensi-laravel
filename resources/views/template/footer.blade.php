<footer class="main-footer">
   <!-- To the right -->
   <div class="float-right d-none d-sm-inline">
      My Presence
   </div>
   <!-- Default to the left -->
   <strong>Copyright &copy; 2014-2021 <a href="https://mypresence.com">MyPresence.com</a>.</strong> All rights reserved.
</footer>
</div>