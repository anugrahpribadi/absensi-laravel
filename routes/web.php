<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login/login');
});

route::get('/registrasi', [LoginController::class, 'registrasi'])->name('registrasi');
route::post('/saveregistrasi', [LoginController::class, 'saveregistrasi'])->name('saveregistrasi');
route::get('/home', [HomeController::class, 'index'])->name('home');
route::get('/login', [LoginController::class, 'halamanlogin'])->name('login');
route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');

route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');

route::get('/logout', [LoginController::class, 'logout'])->name('logout');











